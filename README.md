# Guides

## Style Guides

## SQL

Follow these:

### Do

* Use consistent and descriptive identifiers and names.
* Make judicious use of white space and indentation to make code easier to read.
* Include comments in SQL code where necessary. Use the C style opening `/*` and closing `*/` where possible otherwise precede comments with `--` and finish them with a new line.
* Always use uppercase for the reserved keywords like `SELECT` and `WHERE`.

### Don't

* CamelCase — it is difficult to scan quickly.
* Descriptive prefixes or Hungarian notation such as `sp_` or `tbl`.

### Naming Conventions

* Ensure the name is unique and does not exist as a reserved keyword.
* Names must begin with a letter and may not end with an underscore.
* Use underscores where you would naturally include a space in the name.
* Only use letters, numbers and underscores in names.
* Avoid the use of multiple consecutive underscores — these can be hard to read.

For details [sql style guide](http://www.sqlstyle.guide/)

## Git

Always create branch and send Pull Request(PR)/ Merge Request(MR).

### Branch

* Choose short and descriptive name.
* Identifiers from corresponding tickets (issues) are good candidates for branch name.
* Use dashes to separate words.

### Commits

* Each commit should be single logical change. For example, if a patch fixes a bug and optimizes the performance of a feature, split it into two separate commits.

  _Tip: Use `git add -p` to interactively stage specific portions of the modified files._

* Don't split a single logical change to several commits.
* Commit early and often.

### Messages

* The summary line should start with type.

  The type can be **Feat** (new feature), **Fix** (bug fix), **Docs** (documentation change), **Style** (formatting, missing semi colons, etc; no code change), **Refactor** (refactoring code), **Test** (adding tests, refactoring tests; no code change) and **Chore** (updating build tasks, package manager configs; no code change)

  ```text
  Type: Summary line

  Description

  Footer
  ```

* The summary line should be descriptive but succinct. Ideally, it should be no longer than 50 characters. It should be capitalized and written in imperative present tense. It should not end wth a period since it is effectively the commit title.
* Summary line should be followed by a blank line followed by a more thorough description. It should be wrapped to 72 characters, explaining why the change is needed, how it addresses the issue and what side-effects it might have.
* If a commit A depends on commit B, the dependency should be stated in the message of commit A. Use the SHA1 when referring to commits in footer.

More on [Git Style](https://github.com/agis-/git-style-guide) and [Message structure](https://udacity.github.io/git-styleguide/)

## Java

Follow the [guideline](https://google.github.io/styleguide/javaguide.html)

## Objective C/ Swift

Follow the [guideline](https://google.github.io/styleguide/objcguide.xml) for Objective C and [guideline](https://github.com/linkedin/swift-style-guide) for swift

## Python

Follow the [guideline](https://google.github.io/styleguide/pyguide.html)

## Ruby/ Ruby on Rails(RoR)

* Follow the [Ruby guideline](https://github.com/bbatsov/ruby-style-guide)
* Follow the [RoR guideline](https://github.com/bbatsov/rails-style-guide)

## PHP

* Follow the [PHP guideline](https://github.com/procurios/PHP)
* Related [PSR documentation](https://www.php-fig.org/psr/)
